//
//  Deck.swift
//  CardBattleGame
//
//  Created by Vishal Aggarwal on 19/06/17.
//  Copyright © 2017 SAHNAWAJ BISWAS. All rights reserved.
//

import UIKit

/// Holds information on all the Decks and its cards that were created by the Player.
class Deck: NSObject, NSCoding {
  
  var name: String
  var id: Int16
  var cardList: [Card]
  
  init(name: String, id: Int16, cardList: [Card]) {
    self.name = name
    self.id = id
    self.cardList = cardList
  }
  
  required init?(coder aDecoder: NSCoder) {
    guard let name = aDecoder.decodeObject(forKey: "name") as? String, let id = aDecoder.decodeObject(forKey: "id") as? Int16, let cardList = aDecoder.decodeObject(forKey: "cardList") as? [Card] else {
      return nil
    }
    self.name = name
    self.id = id
    self.cardList = cardList
  }
  
  func encode(with coder: NSCoder) {
    coder.encode(name, forKey: "name")
    coder.encode(id, forKey: "id")
    coder.encode(cardList, forKey: "cardList")
  }
}
