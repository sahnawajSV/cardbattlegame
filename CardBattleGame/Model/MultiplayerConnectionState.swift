//
//  MultiplayerConnectionState.swift
//  CardBattleGame
//
//  Created by SAHNAWAJ BISWAS on 27/07/17.
//  Copyright © 2017 SAHNAWAJ BISWAS. All rights reserved.
//

import UIKit

class MultiplayerConnectionState: NSObject {
  @objc
  enum GameState: Int {
    case offline
    case searchOnlinePlayer
    case foundOnlinePlayer
    case playerConnected
    case playerDisconnected
    case gameEnded
  }
  
  dynamic var state = GameState.offline
}
