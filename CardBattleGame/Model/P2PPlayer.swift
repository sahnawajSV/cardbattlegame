//
//  P2PPlayer.swift
//  CardBattleGame
//
//  Created by SAHNAWAJ BISWAS on 20/07/17.
//  Copyright © 2017 SAHNAWAJ BISWAS. All rights reserved.
//
import MultipeerConnectivity

struct P2PPlayer: Equatable {
  var peerId: MCPeerID
  
  init(peerId: MCPeerID) {
    self.peerId = peerId
  }
  
  public static func ==(lhs: P2PPlayer, rhs: P2PPlayer) -> Bool {
    return lhs.peerId == rhs.peerId
  }
}
