//
//  AllCardsAttack.swift
//  CardBattleGame
//
//  Created by SAHNAWAJ BISWAS on 14/08/17.
//  Copyright © 2017 SAHNAWAJ BISWAS. All rights reserved.
//

import UIKit

class AllCardsAttack: NSObject {
  override init() {
    super.init()
  }
  
  func encode(with aCoder: NSCoder) {
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init()
  }
}

extension AllCardsAttack: GameCommand {
  func execute(model: P2PBehaviourManager) {
    model.playerTwoStats.gameStats.allowAllPlayCardsToAttack()
  }
}
