//
//  GameCommand.swift
//  CardBattleGame
//
//  Created by SAHNAWAJ BISWAS on 11/08/17.
//  Copyright © 2017 SAHNAWAJ BISWAS. All rights reserved.
//
import UIKit

protocol GameCommand: NSCoding {
  func execute(model: P2PBehaviourManager)
}
