//
//  DrawACard.swift
//  CardBattleGame
//
//  Created by SAHNAWAJ BISWAS on 16/08/17.
//  Copyright © 2017 SAHNAWAJ BISWAS. All rights reserved.
//

import UIKit

class DrawACard: NSObject {
  override init() {
    super.init()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init()
  }
  
  func encode(with aCoder: NSCoder) {
  }
}

extension DrawACard: GameCommand {
  func execute(model: P2PBehaviourManager) {
    model.endTurn()
  }
}
