//
//  AttackCard.swift
//  CardBattleGame
//
//  Created by SAHNAWAJ BISWAS on 14/08/17.
//  Copyright © 2017 SAHNAWAJ BISWAS. All rights reserved.
//

import UIKit

class AttackCard: NSObject {
  
  let fromIndex: Int16
  let toIndex: Int16
  let atkCard: Card
  let defCard: Card
  
  init(fromIndex: Int, toIndex: Int, atkCard: Card, defCard: Card) {
    self.fromIndex = Int16(fromIndex)
    self.toIndex = Int16(toIndex)
    self.atkCard = atkCard
    self.defCard = defCard
  }
  
  required init?(coder aDecoder: NSCoder) {
    guard let fromIndex = aDecoder.decodeObject(forKey: "fromIndex") as? Int16, let toIndex = aDecoder.decodeObject(forKey: "toIndex") as? Int16, let atkCard = aDecoder.decodeObject(forKey: "atkCard") as? Card, let defCard = aDecoder.decodeObject(forKey: "defCard") as? Card else {
      return nil
    }
    self.fromIndex = fromIndex
    self.toIndex = toIndex
    self.atkCard = atkCard
    self.defCard = defCard
    super.init()
  }
  
  func encode(with coder: NSCoder) {
    coder.encode(fromIndex, forKey: "fromIndex")
    coder.encode(toIndex, forKey: "toIndex")
    coder.encode(atkCard, forKey: "atkCard")
    coder.encode(defCard, forKey: "defCard")
  }
}

extension AttackCard: GameCommand {
  func execute(model: P2PBehaviourManager) {
    model.attackCard(attacker: atkCard, defender: defCard, fromIndex: Int(fromIndex), toIndex: Int(toIndex))
  }
}
