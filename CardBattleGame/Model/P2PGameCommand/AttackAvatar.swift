//
//  AttackAvatar.swift
//  CardBattleGame
//
//  Created by SAHNAWAJ BISWAS on 16/08/17.
//  Copyright © 2017 SAHNAWAJ BISWAS. All rights reserved.
//

import UIKit

class AttackAvatar: NSObject {
  let indexPath: Int16
  
  init(_ cardIndex: Int) {
    self.indexPath = Int16(cardIndex)
  }
  
  required init?(coder aDecoder: NSCoder) {
    guard let indexPath = aDecoder.decodeObject(forKey: "indexPath") as? Int16 else {
      return nil
    }
    self.indexPath = indexPath
    super.init()
  }
  
  func encode(with coder: NSCoder) {
    coder.encode(indexPath, forKey: "indexPath")
  }
}

extension AttackAvatar: GameCommand {
  func execute(model: P2PBehaviourManager) {
    model.attack(avatar: Int(indexPath))
  }
}
