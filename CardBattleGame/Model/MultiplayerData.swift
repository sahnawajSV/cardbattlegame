//
//  MultiplayerData.swift
//  CardBattleGame
//
//  Created by SAHNAWAJ BISWAS on 03/08/17.
//  Copyright © 2017 SAHNAWAJ BISWAS. All rights reserved.
//

import UIKit

enum ActionType: String {
  case startBattle
  case playerSelectedDeck
  case playerDeckRecived
}

class MultiplayerData: NSObject, NSCoding {
  var dataType: ActionType
  var data: Data
  
  init(with dataType: ActionType, data: Data) {
    self.dataType = dataType
    self.data = data
  }
  
  convenience init(with dataType: ActionType) {
    self.init(with: dataType, data: Data())
  }
  
  required convenience init?(coder aDecoder: NSCoder) {
    guard let dataType = aDecoder.decodeObject(forKey: "dataType") as? String, let dataTypeRaw = ActionType.init(rawValue: dataType), let data = aDecoder.decodeObject(forKey: "data") as? Data else {
      return nil
    }
    self.init(with: dataTypeRaw, data: data)
  }
  
  func encode(with coder: NSCoder) {
    coder.encode(dataType.rawValue, forKey: "dataType")
    coder.encode(data, forKey: "data")
  }
}
