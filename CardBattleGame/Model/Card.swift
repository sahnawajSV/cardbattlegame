//
//  Card.swift
//  CardBattleGame
//
//  Created by SAHNAWAJ BISWAS on 16/06/17.
//  Copyright © 2017 SAHNAWAJ BISWAS. All rights reserved.
//


/// Card Model class to hold data for every individual class
import UIKit

class Card: NSObject, NSCoding {
  
  var name: String
  var id: Int16
  var attack: Int16
  var battlepoint: Int16
  var health: Int16
  var imageName: String
  var canAttack: Bool
  var quantity: Int16
  
  init?(dictionary: [String: Any]) {
    
    guard let name = dictionary["name"] as? String,
      let id = dictionary["id"] as? Int16,
      let attack = dictionary["attack"]  as? Int16,
      let battlepoint = dictionary["battlepoint"] as? Int16,
      let health = dictionary["health"]  as? Int16,
      let imageName = dictionary["imageName"]  as? String,
      let canAttack = dictionary["canAttack"]  as? Bool,
      let quantity = dictionary["quantity"] as? Int16 else {
        return nil
    }
    
    self.name = name
    self.id = id
    self.attack = attack
    self.battlepoint = battlepoint
    self.health = health
    self.imageName = imageName
    self.canAttack = canAttack
    self.quantity = quantity
  }
  
  init(name: String, id: Int16, attack: Int16, battlepoint: Int16, health: Int16, canAttack: Bool, imageName: String, quantity: Int16) {
    self.name = name
    self.id = id
    self.attack = attack
    self.battlepoint = battlepoint
    self.health = health
    self.canAttack = canAttack
    self.imageName = imageName
    self.quantity = quantity
  }
  
  required init?(coder aDecoder: NSCoder) {
    guard let name = aDecoder.decodeObject(forKey: "name") as? String, let id = aDecoder.decodeObject(forKey: "id") as? Int16, let attack = aDecoder.decodeObject(forKey: "attack") as? Int16, let battlepoint = aDecoder.decodeObject(forKey: "battlepoint") as? Int16, let health = aDecoder.decodeObject(forKey: "health") as? Int16, let quantity = aDecoder.decodeObject(forKey: "quantity") as? Int16, let imageName = aDecoder.decodeObject(forKey: "imageName") as? String else {
      return nil
    }
    self.name = name
    self.id = id
    self.attack = attack
    self.battlepoint = battlepoint
    self.health = health
    self.canAttack = aDecoder.decodeBool(forKey: "canAttack")
    self.imageName = imageName
    self.quantity = quantity
  }
  
  func encode(with coder: NSCoder) {
    coder.encode(name, forKey: "name")
    coder.encode(id, forKey: "id")
    coder.encode(attack, forKey: "attack")
    coder.encode(battlepoint, forKey: "battlepoint")
    coder.encode(health, forKey: "health")
    coder.encode(canAttack, forKey: "canAttack")
    coder.encode(quantity, forKey: "quantity")
    coder.encode(imageName, forKey: "imageName")
  }
  
}
