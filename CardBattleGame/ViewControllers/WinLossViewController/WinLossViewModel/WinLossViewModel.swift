//
//  WinLossViewModel.swift
//  CardBattleGame
//
//  Created by SAHNAWAJ BISWAS on 26/07/17.
//  Copyright © 2017 SAHNAWAJ BISWAS. All rights reserved.
//

import UIKit

class WinLossViewModel: NSObject {
  private var playerOwnedCards: [Card] = []
  private var winningCards: [Card] = []
  
  private let coreDataManager = CoreDataStackManager.sharedInstance
  
  override init() {
    super.init()
    winningCards = createWinCards()
    addPlayerWinCardsToStorage()
  }
  
  /// Add wining Cards to to Player Woned Cards
  private func addPlayerWinCardsToStorage() {
    do {
      try winningCards.forEach({ card in
        let playerCards = try coreDataManager.fetchPlayerOwnedCard(for: card.id)
        if let playerCard = playerCards.first {
          playerCard.quantity = playerCard.quantity + 1
          guard let name = playerCard.name, let image = playerCard.imageName else {
            return
          }
          let newCard = Card(name: name, id: playerCard.id, attack: playerCard.attack, battlepoint: playerCard.battlepoint, health: playerCard.health, canAttack: playerCard.canAttack, imageName: image, quantity: playerCard.quantity)
          try coreDataManager.add(playerOwned: newCard)
        } else {
          try coreDataManager.add(playerOwned: card)
        }
      })
    } catch {
      CBGErrorHandler.handle(error: ErrorType.failedManagedObjectFetchRequest)
    }
  }
  
  
  /// Randomly create cards as a reward
  ///
  /// - Returns: Winning Cards
  private func createWinCards() -> [Card] {
      let globalCardData = CardListDataSource()
      let cardList = globalCardData.fetchCardList()
      return cardList.subArray(size: Game.cardsAwardedOnWin)
  }
  
  
  /// Return winning cards
  ///
  /// - Returns: cards
  func fetchWinningCards() -> [Card] {
    return winningCards
  }
}
