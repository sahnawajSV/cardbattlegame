//
//  P2PInviteViewModel.swift
//  CardBattleGame
//
//  Created by SAHNAWAJ BISWAS on 19/07/17.
//  Copyright © 2017 SAHNAWAJ BISWAS. All rights reserved.
//

import UIKit
import MultipeerConnectivity

enum MatchType {
  case OfflineMatch
  case OnlineMatch
}

protocol P2PInviteViewModelDelegate: class {
  
  func updateOponent(player deck: Deck)
  func startBattle()
  func searchingForOnlinePlayer()
  func foundAndConnected(with player: String)
  func connectionLost()
}

class P2PInviteViewModel {
  
  fileprivate var selectedDeck: Data!
  
  fileprivate var onlinePlayers: [P2PPlayer] = []
  let p2pServiceManager = P2PManager.sharedInstance
  
  var delegate : P2PInviteViewModelDelegate?
  
  init() {
    p2pServiceManager.delegate = self
  }
  
  
  /// Check for online player
  func checkForOnlinePlayers() {
    p2pServiceManager.startPeer2PeerService()
  }
  
  /// Convert Deck to Archive Data
  ///
  /// - Parameter deck: Player Selected Deck
  func convertDeckToArchiveData(deck: Deck) {
    selectedDeck = NSKeyedArchiver.archivedData(withRootObject: deck)
  }
  
  /// Create AI deck with random cards for offline play
  func createAIDeck() {
    let globalCardData = CardListDataSource()
    let cardList = globalCardData.fetchCardList()
    let aiCardArray: [Card]  = cardList.subArray(size: Game.maximumCardPerDeck)
    let playerDeck = Deck(name: "Deck_1", id: 1, cardList: aiCardArray)
    delegate?.updateOponent(player: playerDeck)
    delegate?.startBattle()
  }
  
  func shareDeck() {
    let multiPlayerData = MultiplayerData.init(with: ActionType.playerSelectedDeck, data: selectedDeck)
    p2pServiceManager.send(data: multiPlayerData)
  }
  
  func p2pConnectionDisconnected() {
    delegate?.connectionLost()
  }
  
  func startP2PBattle() {
    let multiPlayerData = MultiplayerData.init(with: ActionType.startBattle)
    p2pServiceManager.send(data: multiPlayerData)
  }
}

extension P2PInviteViewModel: P2PManagerDelegate {
  func commandReceivedFromConnectedPlayer(_ manager: P2PManager, command: GameCommand) {
  }

  /// Check Status of P2P player
  ///
  /// - Parameters:
  ///   - manager: P2PManager Object
  ///   - connectedDevices: connectedDevices
  ///   - state: Connection State
  func connectedDevicesChanged(_ manager: P2PManager, connectedDevices: [String], didChange state: MultiplayerConnectionState.GameState) {
    switch state {
    case .searchOnlinePlayer:
       delegate?.searchingForOnlinePlayer()
    case .playerConnected:
      delegate?.foundAndConnected(with: connectedDevices[0])
      shareDeck()
    case .playerDisconnected:
      p2pConnectionDisconnected()
    default:
      print("Offline")
    }
  }         
  
  
  /// Recived Notification with data from connected player
  ///
  /// - Parameters:
  ///   - manager: P2PManager object
  ///   - notification: Dictionary containing notification type with data
  func notificationReceivedFromConnectedPlayer(_ manager: P2PManager, notification: MultiplayerData) {
   let actionState = notification.dataType
    print(actionState.rawValue)
    switch actionState {
    case ActionType.playerSelectedDeck:
      if let playerBDeck: Deck = NSKeyedUnarchiver.unarchiveObject(with: notification.data) as? Deck {
        delegate?.updateOponent(player: playerBDeck)
        let multiPlayerData = p2pServiceManager.isServer ? MultiplayerData.init(with: ActionType.startBattle) : MultiplayerData.init(with: ActionType.playerDeckRecived)
        p2pServiceManager.send(data: multiPlayerData)
      }
    case ActionType.playerDeckRecived:
      delegate?.startBattle()
    case ActionType.startBattle:
      delegate?.startBattle()
    default:
      break
    }
  }
}

