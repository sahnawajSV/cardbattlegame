//
//  PlayerViewController.swift
//  CardBattleGame
//
//  Created by SAHNAWAJ BISWAS on 26/07/17.
//  Copyright © 2017 SAHNAWAJ BISWAS. All rights reserved.
//

import UIKit

class PlayerViewController: UIViewController {
    
  @IBOutlet weak var playerTwoNameLbl: UILabel!
  @IBOutlet weak var playerOneNameLbl: UILabel!
  @IBOutlet weak var playerTwoImage: CircleUIImage!
  @IBOutlet weak var playerOneImage: CircleUIImage!
  @IBOutlet weak var searchingView: UIView!
  @IBOutlet weak var baseView: UIView!
  @IBOutlet weak var playerTwoView: UIView!
  @IBOutlet weak var playerOneView: UIView!
  @IBOutlet weak var loadingView: UIActivityIndicatorView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.playerTwoView.isHidden = true
  }
  
  func onlinePlayerView(with name: String) {
    self.searchingView.isHidden = true
    self.loadingView.stopAnimating()
    UIView.animate(withDuration: 0.5, delay: 0.5, options: UIViewAnimationOptions.curveEaseInOut, animations: {
      self.playerTwoImage.image = #imageLiteral(resourceName: "player_image")
      self.playerTwoNameLbl.text = name
      self.playerTwoView.isHidden = false
    }) { (bool) in
      
    }
  }
}
