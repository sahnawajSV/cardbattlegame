//
//  OnlinePlayersHorizontalScrollView.swift
//  CardBattleGame
//
//  Created by SAHNAWAJ BISWAS on 19/07/17.
//  Copyright © 2017 SAHNAWAJ BISWAS. All rights reserved.
//

import UIKit

protocol OnlinePlayersHorizontalScrollViewDelegate: class {
  // ask the delegate how many views he wants to present inside the horizontal scroller
  func numberOfViewsForHorizontalScroller(_ scroller: OnlinePlayersHorizontalScrollView) -> Int
  // ask the delegate to return the view that should appear at <index>
  func horizontalScrollerViewAtIndex(_ scroller: OnlinePlayersHorizontalScrollView, index:Int) -> UIView
  // inform the delegate what the view at <index> has been clicked
  func horizontalScrollerClickedViewAtIndex(_ scroller: OnlinePlayersHorizontalScrollView, index:Int)
  // ask the delegate for the index of the initial view to display. this method is optional
  // and defaults to 0 if it's not implemented by the delegate
  func initialViewIndex(_ scroller: OnlinePlayersHorizontalScrollView) -> Int
}


class OnlinePlayersHorizontalScrollView: UIView {
  
  
  weak var delegate: OnlinePlayersHorizontalScrollViewDelegate?
  
  fileprivate let viewPadding = 10
  fileprivate let viewDimension = 240
  fileprivate let viewOffset = 300
  
  fileprivate var scroller : UIScrollView!
  
  var viewArray = [UIView]()
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    initializeScrollView()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    initializeScrollView()
  }
  
  
  func initializeScrollView() {
    scroller = UIScrollView()
    scroller.delegate = self
    scroller.backgroundColor = .brown
    addSubview(scroller)
    
    scroller.translatesAutoresizingMaskIntoConstraints = false
    
    self.addConstraint(NSLayoutConstraint(item: scroller, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1.0, constant: 0.0))
    self.addConstraint(NSLayoutConstraint(item: scroller, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1.0, constant: 0.0))
    self.addConstraint(NSLayoutConstraint(item: scroller, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1.0, constant: 0.0))
    self.addConstraint(NSLayoutConstraint(item: scroller, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1.0, constant: 0.0))

    let tapRecognizer = UITapGestureRecognizer(target: self, action:#selector(OnlinePlayersHorizontalScrollView.scrollerTapped(_:)))
    scroller.addGestureRecognizer(tapRecognizer)
    
    print(frame.height)
  }
  
  func viewAtIndex(_ index :Int) -> UIView {
    return viewArray[index]
  }
  
  func scrollerTapped(_ gesture: UITapGestureRecognizer) {
    let location = gesture.location(in: gesture.view)
    if let delegate = delegate {
      for index in 0..<delegate.numberOfViewsForHorizontalScroller(self) {
        let view = scroller.subviews[index]
        if view.frame.contains(location) {
          delegate.horizontalScrollerClickedViewAtIndex(self, index: index)
          scroller.setContentOffset(CGPoint(x: view.frame.origin.x - self.frame.size.width/2 + view.frame.size.width/2, y: 0), animated:true)
          break
        }
      }
    }
  }
  
  override func didMoveToSuperview() {
    super.didMoveToSuperview()
    reload()
  }
  
  func reload() {
    if let delegate = delegate {
      viewArray = []
      let views: [UIView] = scroller.subviews
      
      for view in views {
        view.removeFromSuperview()
      }
      
      var xValue = viewOffset
      for index in 0..<delegate.numberOfViewsForHorizontalScroller(self) {
        xValue += viewPadding
        
        let view = delegate.horizontalScrollerViewAtIndex(self, index: index)
        view.frame = CGRect(x: CGFloat(xValue), y: 0, width: view.frame.width, height: view.frame.height)
        scroller.addSubview(view)
        
        xValue += viewDimension + viewPadding
        viewArray.append(view)
      }
      scroller.contentSize = CGSize(width: CGFloat(xValue + viewOffset), height: frame.size.height)
      
      let newContentOffsetX: CGFloat = (scroller.contentSize.width - scroller.bounds.size.width)/2
      scroller.contentOffset = CGPoint(x: newContentOffsetX, y: 0);
    }
  }
  
  func centerCurrentView() {
    var xFinal = Int(scroller.contentOffset.x) + (viewOffset/2) + viewPadding
    let viewIndex = xFinal / (viewDimension + (2*viewPadding))
    xFinal = viewIndex * (viewDimension + (2*viewPadding))
    scroller.setContentOffset(CGPoint(x: xFinal, y: 0), animated: true)
    
    if let delegate = delegate {
      delegate.horizontalScrollerClickedViewAtIndex(self, index: Int(viewIndex))
    }
  }
}

extension OnlinePlayersHorizontalScrollView: UIScrollViewDelegate {
  func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
    if !decelerate {
      centerCurrentView()
    }
  }
  
  func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
    centerCurrentView()
  }
}
