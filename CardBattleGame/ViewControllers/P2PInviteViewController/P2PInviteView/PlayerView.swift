//
//  PlayerView.swift
//  CardBattleGame
//
//  Created by SAHNAWAJ BISWAS on 19/07/17.
//  Copyright © 2017 SAHNAWAJ BISWAS. All rights reserved.
//

import UIKit

class PlayerView: UIView {
  
  let imageWidth: CGFloat = 160.0
  let imageHeight: CGFloat = 160.0
  
  var playerImageView: CircleUIImage!
  var playerNameLbl: UILabel!
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    commonInit()
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    commonInit()
  }
  
  func commonInit() {
    playerImageView = CircleUIImage()
    addSubview(playerImageView)
    playerImageView.translatesAutoresizingMaskIntoConstraints = false
    
    self.addConstraint(NSLayoutConstraint(item: playerImageView, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1.0, constant: 0.0))
    self.addConstraint(NSLayoutConstraint(item: playerImageView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1.0, constant: 10.0))
    self.addConstraint(NSLayoutConstraint(item: playerImageView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: imageWidth))
    self.addConstraint(NSLayoutConstraint(item: playerImageView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: imageHeight))

    
    playerNameLbl = UILabel()
    playerNameLbl.numberOfLines = 0
    playerNameLbl.textColor = .white
    playerNameLbl.textAlignment = .center
    playerNameLbl.font = UIFont(name: "Helvetica-Bold", size: 21)
    addSubview(playerNameLbl)
    
    playerNameLbl.translatesAutoresizingMaskIntoConstraints = false
    
    self.addConstraint(NSLayoutConstraint(item: playerNameLbl, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1.0, constant: 10.0))
    self.addConstraint(NSLayoutConstraint(item: playerNameLbl, attribute: .top, relatedBy: .equal, toItem: playerImageView, attribute: .bottom, multiplier: 1.0, constant: 10.0))
    self.addConstraint(NSLayoutConstraint(item: playerNameLbl, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1.0, constant: -10.0))
    self.addConstraint(NSLayoutConstraint(item: playerNameLbl, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1.0, constant: 10.0))
  }

  
  func highlightAlbum(didHighlightView: Bool) {
    if didHighlightView == true {
      backgroundColor = UIColor.white
    } else {
      backgroundColor = UIColor.black
    }
  }

}
