//
//  P2PInviteViewController.swift
//  CardBattleGame
//
//  Created by SAHNAWAJ BISWAS on 19/07/17.
//  Copyright © 2017 SAHNAWAJ BISWAS. All rights reserved.
//

import UIKit

class P2PInviteViewController: UIViewController {
  
  var playerADeck: Deck!
  var playerBDeck: Deck!
  
  private let p2pInviteViewModel = P2PInviteViewModel()
  fileprivate var playerViewController: PlayerViewController!
  @IBOutlet weak var playerViewContainer: UIView!
  
  fileprivate var matchType = MatchType.OfflineMatch
  
  @IBOutlet weak var playButtonStackView: UIStackView!
  
  private let battleSegueIdentifier = "battleIndentifier"
  private let onlinePlayerViewIdentefier = "OnlinePlayerView"
  
  override func viewDidLoad() {
    super.viewDidLoad()
    p2pInviteViewModel.delegate = self
    p2pInviteViewModel.convertDeckToArchiveData(deck: playerADeck)
    
    playerViewContainer.alpha = 0
  }
  
  // MARK: - Navigation
  // In a storyboard-based application, you will often want to do a little preparation before navigation
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    guard let identifier = segue.identifier else {
      return
    }
    switch identifier {
    case battleSegueIdentifier:
      if let battleSystemVC = segue.destination as? BattleViewController {
        battleSystemVC.playerADeck = playerADeck
        battleSystemVC.playerBDeck = playerBDeck
        battleSystemVC.isMultiplayerEnabled = matchType == MatchType.OnlineMatch ? true : false
      }
    default:
      if let playController = segue.destination as? PlayerViewController {
        playerViewController = playController
      }
    }
    
  }
  
  @IBAction func playOfflineAction(_ sender: Any) {
    matchType = MatchType.OfflineMatch
    p2pInviteViewModel.createAIDeck()
  }
  
  @IBAction func playOnlineAction(_ sender: Any) {
    matchType = MatchType.OnlineMatch
    p2pInviteViewModel.checkForOnlinePlayers()
  }
  
  func performSegue() {
    performSegue(withIdentifier: battleSegueIdentifier, sender: index)
  }
  
  @IBAction func backAction(_ sender: Any) {
    navigationController?.popViewController(animated: true)
  }
}

extension P2PInviteViewController: P2PInviteViewModelDelegate {
  
  func updateOponent(player deck: Deck) {
    playerBDeck = deck
  }
  
  func startBattle() {
    DispatchQueue.main.async {
      self.performSegue()
    }
  }
  
  func searchingForOnlinePlayer() {
    DispatchQueue.main.async {
      UIView.animate(withDuration: 0.5, delay: 0.5, options: UIViewAnimationOptions.curveEaseInOut, animations: {
        self.playButtonStackView.isHidden = true
        self.playerViewContainer.alpha = 1
      }) { (bool) in
        
      }
    }
  }
  
  func foundAndConnected(with player: String) {
    DispatchQueue.main.async {
      self.playerViewController.onlinePlayerView(with: player)
    }
  }
  
  func connectionLost() {
    DispatchQueue.main.async {
      let alertController =  UIAlertController(title: "Error", message: "Sorry, Connection Lost", preferredStyle: UIAlertControllerStyle.alert)
      let okAction  = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) in
        self.navigationController?.popToRootViewController(animated: true)
      }
      alertController.addAction(okAction)
      self.present(alertController, animated: true, completion: nil)
    }
  }
}

