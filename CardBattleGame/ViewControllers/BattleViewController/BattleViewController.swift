//
//  BattleViewController.swift
//  CardBattleGame
//
//  Created by Vishal Aggarwal on 07/07/17.
//  Copyright © 2017 SAHNAWAJ BISWAS. All rights reserved.
//

import UIKit
import AVFoundation

class BattleViewController: UIViewController, BattleDelegate, InHandViewControllerDelegate, InPlayViewControllerDelegate {
  
  //Decks sent by Deck Selector. It automatically checks for multiplayer/single player game and sends the deck accordingly
  var playerADeck: Deck!
  var playerBDeck: Deck!
  
  //ViewModel Class
  private var bViewModel = BattleViewModel()
  
  //Action Is finished Check
  var isPerformingAnAction: Bool = false
  
  //Multiplayer Checker
  var isMultiplayerEnabled: Bool = false
  
  //To Play Background Music
  var player: AVAudioPlayer?
  var effectPlayer: AVAudioPlayer?
  
  //MARK: - CardView Collection
  var allPlayerOneInHandCards: [CardView] = []
  var allPlayerTwoInHandCards: [CardView] = []
  var allPlayerOneInPlayCards: [CardView] = []
  var allPlayerTwoInPlayCards: [CardView] = []
  
  //MARK: - Storyboard Connections
  @IBOutlet private weak var playerOneInDeckText: UILabel!
  @IBOutlet private weak var playerOneBattlePointText: UILabel!
  @IBOutlet private weak var playerOneNameText: UILabel!
  @IBOutlet private weak var playerOneHealthText: UILabel!
  
  @IBOutlet private weak var playerTwoInDeckText: UILabel!
  @IBOutlet private weak var playerTwoBattlePointText: UILabel!
  @IBOutlet private weak var playerTwoNameText: UILabel!
  @IBOutlet private weak var playerTwoHealthText: UILabel!
  
  @IBOutlet private weak var playerOneView: UIView!
  @IBOutlet var playerOneDeckView: UIView!
  
  @IBOutlet private weak var playerTwoView: UIView!
  @IBOutlet weak var playerTwoDeckView: UIView!
  
  @IBOutlet private weak var playerTwoHandContainer: UIView!
  @IBOutlet private weak var playerTwoPlayContainer: UIView!
  @IBOutlet private weak var playerOneHandContainer: UIView!
  @IBOutlet private weak var playerOnePlayContainer: UIView!
  
  @IBOutlet private weak var turnIndicatorView: UIImageView!
  
  @IBOutlet private weak var viewBG: UIImageView!
  
  @IBOutlet private weak var endTurnButton: UIButton!
  
  //Handles all InHand and InPlay CardViews
  private var playerOneInHandController: InHandViewController!
  private var playerOnePlayController: InPlayViewController!
  private var playerTwoInHandController: InHandViewController!
  private var playerTwoPlayController: InPlayViewController!
  
  //UI Logic Handlers
  private var attackCardIndex: Int?
  private var winLossReached: Bool = false
  private var timer: Timer?
  private var count = Game.turnTimer
  
  //MARK: - UIView Methods
  override func viewDidLoad() {
    super.viewDidLoad()
    if isMultiplayerEnabled {
      bViewModel = P2PViewModel()
    } else {
      bViewModel = AIViewModel()
    }
    bViewModel.delegate = self
    playerOneInHandController.delegate = self
    playerTwoInHandController.delegate = self
    playerOnePlayController.delegate = self
    playerTwoPlayController.delegate = self
    bViewModel.initializeTheGame(playerOne: playerADeck, playerTwo: playerBDeck)
    playBackgroundMucic(musicFileName: "background_music")
  }
  
  //MARK: Class Helpers
  private func playBackgroundMucic(musicFileName: String) {
//    if let sound = NSDataAsset(name: musicFileName) {
//      do {
//        player = try AVAudioPlayer(data: sound.data, fileTypeHint: "mp4")
//        player?.numberOfLoops = -1
//        player?.volume = Float(Game.backgroundMusicVolume)
//        player?.prepareToPlay()
//        player?.play()
//      } catch {
//        
//      }
//    }
  }
  
  private func playSoundEffect(musicFileName: String) {
//    if let sound = NSDataAsset(name: musicFileName) {
//      do {
//        effectPlayer = try AVAudioPlayer(data: sound.data, fileTypeHint: "mp3")
//        effectPlayer?.volume = Float(Game.attackMusicVolume)
//        effectPlayer?.prepareToPlay()
//        effectPlayer?.play()
//      } catch {
//        
//      }
//    }
  }
  
  private func reloadAllUIElements() {
    playerOneInDeckText.text = bViewModel.playerOneNumOfCardsInDeckText
    playerTwoInDeckText.text = bViewModel.playerTwoNumOfCardsInDeckText
    
    playerOneBattlePointText.text = bViewModel.playerOneTotalBattlePointsText
    playerTwoBattlePointText.text = bViewModel.playerTwoTotalBattlePointsText
    
    playerOneNameText.text = bViewModel.playerOneName
    playerTwoNameText.text = bViewModel.playerTwoName
    
    playerOneHealthText.text = bViewModel.playerOneHealth
    playerTwoHealthText.text = bViewModel.playerTwoHealth
  }
  
  private func handleInhandCards(allCards: [Card], inHandViewController: InHandViewController, allCardViews: inout [CardView]) {
    allCardViews.removeAll()
    isPerformingAnAction = true
    let frame = getPlayerDeckViewFrame(inHandViewController: inHandViewController, forDeckView: playerOneDeckView)
    allCardViews = inHandViewController.createCards(allCards: allCards, fromFrame: frame)
  }
  
  private func handleDrawingOfNewCard(allCards: [Card], inHandViewController: InHandViewController, allCardViews: inout [CardView]) {
    if allCardViews.count < 5 {
      isPerformingAnAction = true
      var frame: CGRect = CGRect.zero
      if bViewModel.isPlayerTurn {
        frame = getPlayerDeckViewFrame(inHandViewController: inHandViewController, forDeckView: playerOneDeckView)
      } else {
        frame = getPlayerDeckViewFrame(inHandViewController: inHandViewController, forDeckView: playerTwoDeckView)
      }
      if let newCard = allCards.last {
        allCardViews = inHandViewController.createACard(card: newCard, fromFrame: frame, cardIndex: allCards.count-1)
      }
    } else {
      if !bViewModel.isPlayerTurn {
        bViewModel.startPlayerTwoChecks()
      }
    }
  }
  
  private func handleCardToCardAttack(atkIndex: Int, defIndex: Int, atkCard: Card, defCard: Card) {
    isPerformingAnAction = true
    let updatedAttackerHealth: Int16 = atkCard.health - defCard.attack
    let updatedDefenderHealth: Int16 = defCard.health - atkCard.attack
    if bViewModel.isPlayerTurn {
      let atkCardView: CardView = allPlayerOneInPlayCards[atkIndex]
      let defCardView: CardView = allPlayerTwoInPlayCards[defIndex]
      atkCardView.changeCardState(cardState: .cannotAttack)
      let success = bViewModel.performAttackOnCard(atkIndex: atkIndex, defIndex: defIndex)
      if success {
        performCardToCardAttack(atkView: atkCardView, defView: defCardView, attackerPlayViewController: playerOnePlayController, defenderPlayViewController: playerTwoPlayController, atkIndex: atkIndex, defIndex: defIndex, atkHealth: updatedAttackerHealth, defHealth: updatedDefenderHealth)
      } else {
        isPerformingAnAction = false
      }
    } else {
      let atkCardView: CardView = allPlayerTwoInPlayCards[atkIndex]
      let defCardView: CardView = allPlayerOneInPlayCards[defIndex]
      atkCardView.changeCardState(cardState: .cannotAttack)
      performCardToCardAttack(atkView: atkCardView, defView: defCardView, attackerPlayViewController: playerTwoPlayController, defenderPlayViewController: playerOnePlayController, atkIndex: atkIndex, defIndex: defIndex, atkHealth: updatedAttackerHealth, defHealth: updatedDefenderHealth)
    }
  }
  
  private func handleCardToAvatarAttack(atkIndex: Int) {
    if bViewModel.isPlayerTurn {
      if allPlayerOneInPlayCards.count >= atkIndex+1 {
        isPerformingAnAction = true
        let atkCardView: CardView = allPlayerOneInPlayCards[atkIndex]
        atkCardView.changeCardState(cardState: .cannotAttack)
        performCardToAvatarAttack(atkView: atkCardView, defView: playerTwoView, inPlayViewController: playerOnePlayController, cardIndex: atkIndex)
      }
    } else {
      if allPlayerTwoInPlayCards.count >= atkIndex+1 {
        isPerformingAnAction = true
        let atkCardView: CardView = allPlayerTwoInPlayCards[atkIndex]
        atkCardView.changeCardState(cardState: .cannotAttack)
        performCardToAvatarAttack(atkView: atkCardView, defView: playerOneView, inPlayViewController: playerTwoPlayController, cardIndex: atkIndex)
      }
    }
  }
  
  func handleCardPlay(cardIndex: Int, allInHandCards: inout [CardView]) {
    if allInHandCards.count >= cardIndex+1 {
      isPerformingAnAction = true
      if bViewModel.isPlayerTurn {
        playCardForPlayer(allInHandCards: &allInHandCards, allInPlayCards: &allPlayerOneInPlayCards, cardView: allInHandCards[cardIndex], inHandIndex: cardIndex, inHandViewController: playerOneInHandController, inPlayViewController: playerOnePlayController, inPlayIndex: bViewModel.playerOneInPlayCards.count-1)
      } else {
          playCardForPlayer(allInHandCards: &allInHandCards, allInPlayCards: &allPlayerTwoInPlayCards, cardView: allInHandCards[cardIndex], inHandIndex: cardIndex, inHandViewController: playerTwoInHandController, inPlayViewController: playerTwoPlayController, inPlayIndex: bViewModel.playerTwoInPlayCards.count-1)
      }
    }
  }
  
  private func handlePlayerOneTurnStart() {
    allPlayerOneInPlayCards.forEach { (cardView) in
      cardView.changeCardState(cardState: .canAttack)
    }
  }
  
  private func handlePlayerTwoTurnStart() {
    allPlayerTwoInPlayCards.forEach { (cardView) in
      cardView.changeCardState(cardState: .canAttack)
    }
  }
  
  //MARK: - Actions
  @IBAction private func endTurnPressed(sender: UIButton) {
    if !isPerformingAnAction {
      view.bringSubview(toFront: playerOneHandContainer)
      view.bringSubview(toFront: playerTwoHandContainer)
      if bViewModel.isPlayerTurn {
        turnIndicatorView.transform = CGAffineTransform(rotationAngle: (-45 * CGFloat(Double.pi)) / 180.0)
        bViewModel.toggleTurn(forPlayerOne: true)
        if let gameTimer = timer {
          gameTimer.invalidate()
        }
        count = Game.turnTimer
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(update), userInfo: nil, repeats: true)
      }
    }
  }
  
  @IBAction private func avatarClicked(sender: UIButton) {
    guard let cardIndex = attackCardIndex else {
      return
    }
    if sender.tag == 1 {
      if bViewModel.isPlayerTurn {
        if bViewModel.playerOneInPlayCards[cardIndex].canAttack {
          bViewModel.performAttackOnAvatar(cardIndex: cardIndex)
          allPlayerOneInPlayCards[cardIndex].changeCardState(cardState: .cannotAttack)
          performCardToAvatarAttack(atkView: allPlayerOneInPlayCards[cardIndex], defView: playerTwoView, inPlayViewController: playerOnePlayController, cardIndex: cardIndex)
        }
      }
    }
  }
  
  //MARK: - Function Helpers
  private func getPlayerDeckViewFrame(inHandViewController: InHandViewController, forDeckView: UIView) -> CGRect {
    return inHandViewController.view.convert(forDeckView.frame, from:view)
  }
  
  private func completedAllAnimations() {
    isPerformingAnAction = false
    attackCardIndex = nil
    view.sendSubview(toBack: playerOneHandContainer)
    view.sendSubview(toBack: playerTwoHandContainer)
    view.sendSubview(toBack: viewBG)
    view.bringSubview(toFront: playerTwoHandContainer)
    view.bringSubview(toFront: playerOneHandContainer)
      resetInHandOrPlayIndex(allCardViews: &allPlayerOneInPlayCards)
      resetInHandOrPlayIndex(allCardViews: &allPlayerTwoInPlayCards)
    if !bViewModel.isPlayerTurn && bViewModel is AIViewModel {
      bViewModel.startPlayerTwoChecks()
    }
  }
  
  
  /// Method to update the Turn Timer for PlayerOne and PlayerTwo
  func update() {
    if(count > 0){
      let minutes = String(count / 60)
      let seconds = String(count % 60)
      endTurnButton.setTitle(minutes + ":" + seconds, for: .normal)
      count -= 1
    } else {
      endTurnButton.setTitle("End Turn", for: .normal)
      if let gameTimer = timer {
        gameTimer.invalidate()
      }
      if bViewModel.isPlayerTurn {
        bViewModel.toggleTurn(forPlayerOne: true)
      } else {
        bViewModel.toggleTurn(forPlayerOne: false)
      }
    }
  }
  
  
  private func performCardToCardAttack(atkView: CardView, defView: UIView, attackerPlayViewController: InPlayViewController, defenderPlayViewController: InPlayViewController, atkIndex: Int, defIndex: Int, atkHealth: Int16, defHealth: Int16) {
    let frame = view.convert(atkView.frame, from:attackerPlayViewController.view)
    let swordImage: UIImageView = UIImageView(frame: CGRect(x: frame.origin.x, y: frame.origin.y, width: 80, height: 160))
    swordImage.image = #imageLiteral(resourceName: "sword")
    view.addSubview(swordImage)
    let angle = degreesToRotateObjectWithPosition(objPos: defView.center, andTouchPoint: swordImage.center)
    swordImage.transform = CGAffineTransform(rotationAngle: (angle * CGFloat(Double.pi)) / 180.0)
    let defFrame = view.convert(defView.frame, from:defenderPlayViewController.view)
    playSoundEffect(musicFileName: "katanaSlash")
    UIView.animate(withDuration: Game.cardAttackAnimationSpeed,
                   delay: 0,
                   options: UIViewAnimationOptions.curveEaseIn,
                   animations: { () -> Void in
                    swordImage.frame = CGRect(x: defFrame.origin.x, y: defFrame.origin.y, width: swordImage.frame.size.width, height: swordImage.frame.size.height)
    }, completion: { (finished) -> Void in
      swordImage.removeFromSuperview()
      if self.bViewModel.isPlayerTurn {
        self.performCardHealthCheck(playerCards: &self.allPlayerOneInPlayCards, cardIndex: atkIndex, updatedHealth: atkHealth, inPlayerController: self.playerOnePlayController)
        self.performCardHealthCheck(playerCards: &self.allPlayerTwoInPlayCards, cardIndex: defIndex, updatedHealth: defHealth, inPlayerController: self.playerTwoPlayController)
      } else {
        self.performCardHealthCheck(playerCards: &self.allPlayerTwoInPlayCards, cardIndex: atkIndex, updatedHealth: atkHealth, inPlayerController: self.playerTwoPlayController)
        self.performCardHealthCheck(playerCards: &self.allPlayerOneInPlayCards, cardIndex: defIndex, updatedHealth: defHealth, inPlayerController: self.playerOnePlayController)
      }
      self.completedAllAnimations()
    })
  }
  
  
  private func performCardToAvatarAttack(atkView: CardView, defView: UIView, inPlayViewController: InPlayViewController, cardIndex: Int) {
    let frame = view.convert(atkView.frame, from:inPlayViewController.view)
    let swordImage: UIImageView = UIImageView(frame: CGRect(x: frame.origin.x, y: frame.origin.y, width: 80, height: 160))
    swordImage.image = #imageLiteral(resourceName: "sword")
    view.addSubview(swordImage)
    let angle = degreesToRotateObjectWithPosition(objPos: defView.center, andTouchPoint: swordImage.center)
    swordImage.transform = CGAffineTransform(rotationAngle: (angle * CGFloat(Double.pi)) / 180.0)
    performAttackAnimation(swordView: swordImage, toFrame: defView.frame)
  }
  
  private func performAttackAnimation(swordView: UIImageView, toFrame: CGRect) {
    playSoundEffect(musicFileName: "katanaSlash")
    UIView.animate(withDuration: Game.cardAttackAnimationSpeed,
                   delay: 0,
                   options: UIViewAnimationOptions.curveLinear,
                   animations: { () -> Void in
                    swordView.frame = CGRect(x: toFrame.origin.x, y: toFrame.origin.y, width: swordView.frame.size.width, height: swordView.frame.size.height)
    }, completion: { (finished) -> Void in
      swordView.removeFromSuperview()
      self.reloadAllUIElements()
      self.completedAllAnimations()
    })
  }
  
  private func resetInHandOrPlayIndex(allCardViews: inout [CardView]) {
    allCardViews.enumerated().forEach { (index, cardView) in
      cardView.cardButton.tag = index
    }
  }
  
  private func playCardForPlayer(allInHandCards: inout [CardView], allInPlayCards: inout [CardView], cardView: CardView, inHandIndex: Int, inHandViewController: InHandViewController, inPlayViewController: InPlayViewController, inPlayIndex: Int) {
    let frame = inPlayViewController.view.convert(cardView.frame, from:inHandViewController.view)
    let newCard = createNewCardFromOldCard(oldCardView: cardView)
    inPlayViewController.playACard(cardView: newCard, currentFrame: frame, cardIndex: inPlayIndex)
    allInPlayCards.append(newCard)
    allInHandCards.remove(at: inHandIndex)
    inHandViewController.removeCard(cardIndex: inHandIndex)
    resetInHandOrPlayIndex(allCardViews: &allInHandCards)
    bViewModel.updateData()
  }
  
  private func createNewCardFromOldCard(oldCardView: CardView) -> CardView {
    let newCard: CardView = CardView(frame: playerOnePlayController.cardOne.frame)
    newCard.bpText.text = oldCardView.bpText.text
    newCard.nameText.text = oldCardView.nameText.text
    newCard.attackText.text = oldCardView.attackText.text
    newCard.healthText.text = oldCardView.healthText.text
    newCard.cardImage.image = oldCardView.cardImage.image
    oldCardView.removeFromSuperview()
    return newCard
  }
  
  private func performCardHealthCheck(playerCards: inout [CardView], cardIndex: Int, updatedHealth: Int16, inPlayerController: InPlayViewController) {
    let cardView: CardView = playerCards[cardIndex]
    if updatedHealth <= 0 {
      cardView.removeFromSuperview()
      playerCards.remove(at: cardIndex)
      inPlayerController.removeCard(cardIndex: cardIndex)
    } else {
      cardView.healthText.text = String(updatedHealth)
      playerCards[cardIndex] = cardView
    }
    resetInHandOrPlayIndex(allCardViews: &allPlayerOneInPlayCards)
    resetInHandOrPlayIndex(allCardViews: &allPlayerTwoInPlayCards)
  }
  
  private func degreesToRotateObjectWithPosition(objPos: CGPoint, andTouchPoint: CGPoint) -> CGFloat {
    let dX = andTouchPoint.x - objPos.x
    let dY = andTouchPoint.y - objPos.y
    let bearingRadians = atan2f(Float(dX), Float(dY))
    
    var bearingDegrees = CGFloat(bearingRadians).degrees
    if bearingDegrees < 0 {
      bearingDegrees = abs(bearingDegrees)
    } else {
      bearingDegrees = -bearingDegrees
    }
    
    let degrees: CGFloat = bearingDegrees
    
    return degrees
  }
  
  //MARK: - ViewModel Delegates
  func reloadAllUIElements(_ battleViewModel: BattleViewModel) {
    reloadAllUIElements()
  }
  
  func createInHandCards(_ battleViewModel: BattleViewModel) {
    createInitialInHandCards()
  }
  
  func drawANewCard(_ battleViewModel: BattleViewModel) {
    drawNewCardFromDeck()
  }
  
  //MARK: - Container View Preparation Segue
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    guard let identifier = segue.identifier else {
      return
    }
    
    switch identifier {
    case "PlayerOneInHand":
      if let handController = segue.destination as? InHandViewController {
        playerOneInHandController = handController
        playerOneInHandController.isPlayer = true
      }
      break
    case "PlayerOneInPlay":
      if let playController = segue.destination as? InPlayViewController {
        playerOnePlayController = playController
      }
      break
    case "PlayerTwoInHand":
      if let handController = segue.destination as? InHandViewController {
        playerTwoInHandController = handController
        playerTwoInHandController.isPlayer = false
      }
      break
    case "PlayerTwoInPlay":
      if let playController = segue.destination as? InPlayViewController {
        playerTwoPlayController = playController
      }
      break
    default:
      break
    }
  }
  
  //MARK: - InHandViewController Delegate
  func didCompleteInHandAction(_ inHandViewController: InHandViewController) {
    completedAllAnimations()
  }
  
  func cardSelectedInHandToPlay(_ inHandViewController: InHandViewController, cardIndex: Int) {
    if !isPerformingAnAction && bViewModel.isPlayerTurn {
      let success = bViewModel.playCard(cardIndex: cardIndex)
      if success {
//        if bViewModel.isPlayerTurn {
          handleCardPlay(cardIndex: cardIndex, allInHandCards: &allPlayerOneInHandCards)
//        }
//        else {
//          handleCardPlay(cardIndex: cardIndex, allInHandCards: &allPlayerTwoInHandCards)
//        }
      }
    }
  }
  
  //MARK: - InPlayViewController Delegate
  func didCompleteInPlayAction(_ inPlayViewController: InPlayViewController) {
    completedAllAnimations()
  }
  
  func cardSelectedInPlayToAttack(_ inPlayViewController: InPlayViewController, cardIndex: Int) {
    if bViewModel.isPlayerTurn && !isPerformingAnAction {
      if let index = attackCardIndex {
        if inPlayViewController == playerTwoPlayController && bViewModel.playerOneInPlayCards.count > index && bViewModel.playerTwoInPlayCards.count > cardIndex {
           handleCardToCardAttack(atkIndex: index, defIndex: cardIndex, atkCard: bViewModel.playerOneInPlayCards[index], defCard: bViewModel.playerTwoInPlayCards[cardIndex])
          return
        }
      }
      if inPlayViewController == playerOnePlayController {
        attackCardIndex = cardIndex
      }
    }
  }
  
  //MARK: - BattleViewModel Delegate
  func didSelectCardToPlay(_ battleViewModel: BattleViewModel, cardIndex: Int) {
    handleCardPlay(cardIndex: cardIndex, allInHandCards: &allPlayerTwoInHandCards)
  }
  
  func performCardToCardAttack(_ battleViewModel: BattleViewModel, atkIndex: Int, defIndex: Int, atkCard: Card, defCard: Card) {
      handleCardToCardAttack(atkIndex: atkIndex, defIndex: defIndex, atkCard: atkCard, defCard: defCard)
  }
  
  func performCardToAvatarAttack(_ battleViewModel: BattleViewModel, cardIndex: Int) {
      handleCardToAvatarAttack(atkIndex: cardIndex)
  }
  
  func winLossConditionReached(_ battleViewModel: BattleViewModel, isVictorious: Bool) {
    if !winLossReached {
      winLossReached = true
      let storyboard = UIStoryboard(name: "Main", bundle: nil)
      let winLossViewController = storyboard.instantiateViewController(withIdentifier: "WinLoss") as! WinLossViewController
      winLossViewController.isVictorious = isVictorious
      navigationController?.pushViewController(winLossViewController, animated: true)
    }
  }
  
  func playerTwoDidEndTurn(_ battleViewModel: BattleViewModel) {
    let when = DispatchTime.now() + 1
    DispatchQueue.main.asyncAfter(deadline: when) {
      self.turnIndicatorView.transform = CGAffineTransform(rotationAngle: (45 * CGFloat(Double.pi)) / 180.0)
      self.isPerformingAnAction = false
      self.attackCardIndex = nil
      if let gameTimer = self.timer {
        gameTimer.invalidate()
      }
        self.count = Game.turnTimer
        self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.update), userInfo: nil, repeats: true)
    }
  }
  
  //MARK: - BattleViewModel Delegate Helpers
  private func createInitialInHandCards() {
    handleInhandCards(allCards: bViewModel.playerOneInHandCards, inHandViewController: playerOneInHandController, allCardViews: &allPlayerOneInHandCards)
    handleInhandCards(allCards: bViewModel.playerTwoInHandCards, inHandViewController: playerTwoInHandController, allCardViews: &allPlayerTwoInHandCards)
  }
  
  private func drawNewCardFromDeck() {
    if bViewModel.isPlayerTurn {
      if bViewModel.playerOneInDeckCards.count > 0 || allPlayerOneInHandCards.count < bViewModel.playerOneInHandCards.count {
        view.bringSubview(toFront: playerOneHandContainer)
        handleDrawingOfNewCard(allCards: bViewModel.playerOneInHandCards, inHandViewController: playerOneInHandController, allCardViews: &allPlayerOneInHandCards)
      }
      handlePlayerOneTurnStart()
    } else {
      if bViewModel.playerTwoInDeckCards.count > 0 || allPlayerTwoInHandCards.count < bViewModel.playerTwoInHandCards.count {
        view.bringSubview(toFront: playerTwoHandContainer)
        handleDrawingOfNewCard(allCards: bViewModel.playerTwoInHandCards, inHandViewController: playerTwoInHandController, allCardViews: &allPlayerTwoInHandCards)
        handlePlayerTwoTurnStart()
      } else {
        handlePlayerTwoTurnStart()
        completedAllAnimations()
      }
    }
  }
  
  func multiplayerConnecionLost(_ battleViewModel: BattleViewModel) {
    DispatchQueue.main.async {
      self.player?.stop()
      let alertController =  UIAlertController(title: "Error", message: "Sorry, Connection Lost", preferredStyle: UIAlertControllerStyle.alert)
      let okAction  = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (action) in
        self.navigationController?.popToRootViewController(animated: true)
      }
      alertController.addAction(okAction)
      self.present(alertController, animated: true, completion: nil)
    }
  }
}
