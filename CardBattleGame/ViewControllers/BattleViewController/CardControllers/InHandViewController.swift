//
//  InHandViewController.swift
//  CardBattleGame
//
//  Created by Vishal Aggarwal on 07/07/17.
//  Copyright © 2017 SAHNAWAJ BISWAS. All rights reserved.
//

import UIKit


/// Delegate to BattleViewController
protocol InHandViewControllerDelegate: class {
  func didCompleteInHandAction(_ inHandViewController: InHandViewController)
  func cardSelectedInHandToPlay(_ inHandViewController: InHandViewController, cardIndex: Int)
}

class InHandViewController: UIViewController {
  
  weak var delegate: InHandViewControllerDelegate?
  
  @IBOutlet private weak var cardOne: UIView!
  @IBOutlet private weak var cardTwo: UIView!
  @IBOutlet private weak var cardThree: UIView!
  @IBOutlet private weak var cardFour: UIView!
  @IBOutlet private weak var cardFive: UIView!
  
  private var selectedCardIndex: Int?
  
  private var animActionToPerform: Int = 0
  private var animActionsCompleted: Int = 0
  
  private var allCardViews: [CardView] = []
  
  //Assigned on Prepare Seague as a way to check if the class is working for Player One or for Player Two
  var isPlayer: Bool!
  
  //MARK: - ViewController Calls
  
  /// Creates new Cards
  ///
  /// - Parameters:
  ///   - allCards: Model Array of InHandCards
  ///   - fromFrame: frame to move the card to
  /// - Returns: returns an array of all CardViews created in this method
  func createCards(allCards: [Card], fromFrame: CGRect) -> [CardView] {
    allCardViews.removeAll()
    animActionToPerform = allCards.count
    animActionsCompleted = 0
    allCards.enumerated().forEach { (index, card) in
      let cardView = createACard(card: card, cardIndex: index)
      allCardViews.append(cardView)
      performCardMoveAnimation(cardView: cardView, fromFrame: fromFrame, forIndex: index)
    }
    return allCardViews
  }
  
  
  /// Creates a single card
  ///
  /// - Parameters:
  ///   - card: Card model data
  ///   - fromFrame: frame to move the card to
  ///   - cardIndex: Index at which the card needs to be added in the array
  /// - Returns: returns an array of updated CardViews
  func createACard(card: Card, fromFrame: CGRect, cardIndex: Int) -> [CardView] {
    animActionToPerform = 1
    animActionsCompleted = 0
    let cardView = createACard(card: card, cardIndex: cardIndex)
    allCardViews.append(cardView)
    performCardMoveAnimation(cardView: cardView, fromFrame: fromFrame, forIndex: cardIndex)
    return allCardViews
  }
  
  
  /// Remove a CardView from the UI
  ///
  /// - Parameter cardIndex: Index of the CardView to be removed
  func removeCard(cardIndex: Int) {
    allCardViews.remove(at: cardIndex)
    resetCardPositions()
  }
  

  //MARK: - Action Methods
  func selectInHandCard(button: UIButton) {
    let cardClickedIndex: Int = button.tag
    allCardViews.enumerated().forEach { (index, cardView) in
      let state: CardState = index == cardClickedIndex ? .isSelected : .neutral
      cardView.changeCardState(cardState: state)
    }
    if cardClickedIndex == selectedCardIndex {
      let cardView: CardView = allCardViews[cardClickedIndex]
      cardView.changeCardState(cardState: .neutral)
      delegate?.cardSelectedInHandToPlay(self, cardIndex: cardClickedIndex)
      selectedCardIndex = nil
    } else {
      selectedCardIndex = cardClickedIndex
    }
  }

  
  //Helpers
  private func getCardFrame(forIndex: Int) -> CGRect {
    let cards = [cardOne, cardTwo, cardThree, cardFour, cardFive]
    guard forIndex < cards.count, let card =  cards[forIndex] else {
      return .zero
    }
    return card.frame
  }
  
  private func createACard(card: Card, cardIndex: Int) -> CardView {
    let frame = getCardFrame(forIndex: cardIndex)
    let cardView: CardView = CardView(frame: frame)
    cardView.bpText.text = String(card.battlepoint)
    cardView.attackText.text = String(card.attack)
    cardView.healthText.text = String(card.health)
    cardView.nameText.text = card.name
    cardView.cardButton.tag = cardIndex
    cardView.cardImage.image = UIImage.init(named: card.imageName)
    if isPlayer {
      cardView.cardButton.addTarget(self, action: #selector(selectInHandCard(button:)), for: .touchUpInside)
      toggleHidingOfLabelsOnCard(hideStatus: false, cardView: cardView)
    } else {
      toggleHidingOfLabelsOnCard(hideStatus: true, cardView: cardView)
    }
    cardView.changeCardState(cardState: .neutral)
    DispatchQueue.main.async {
      self.view.addSubview(cardView)
      self.view.layoutIfNeeded()
    }
    return cardView
  }
  
  private func toggleHidingOfLabelsOnCard(hideStatus: Bool, cardView: CardView) {
    cardView.bpView.isHidden = hideStatus
    cardView.healthView.isHidden = hideStatus
    cardView.attackView.isHidden = hideStatus
    cardView.nameView.isHidden = hideStatus
    cardView.cardImage.isHidden = hideStatus
    cardView.cardBack.isHidden = !hideStatus
  }
  
  private func resetCardPositions() {
    animActionToPerform = allCardViews.count
    animActionsCompleted = 0
    allCardViews.enumerated().forEach { (index, cardView) in
      cardView.cardButton.tag = index
      cardView.changeCardState(cardState: .neutral)
      performCardMoveAnimation(cardView: cardView, fromFrame: cardView.frame, forIndex: index)
    }
  }
  
  //MARK: - Animation Helpers
  func performCardMoveAnimation(cardView: CardView, fromFrame: CGRect, forIndex: Int) {
    cardView.frame = fromFrame
    let frame = getCardFrame(forIndex: forIndex)
    UIView.animate(withDuration: Game.cardMoveAnimationSpeed,
                   delay: 0,
                   options: UIViewAnimationOptions.curveEaseIn,
                   animations: { () -> Void in
                    cardView.frame = frame
    }, completion: { (finished) -> Void in
      self.animActionsCompleted += 1
      if self.animActionsCompleted == self.animActionToPerform {
        self.animActionsCompleted = 0
        self.animActionToPerform = 0
        self.delegate?.didCompleteInHandAction(self)
      }
    })
  }
}
