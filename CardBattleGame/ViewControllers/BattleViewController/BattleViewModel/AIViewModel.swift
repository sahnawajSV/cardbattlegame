//
//  AIViewModel.swift
//  CardBattleGame
//
//  Created by Vishal Aggarwal on 01/08/17.
//  Copyright © 2017 SAHNAWAJ BISWAS. All rights reserved.
//

import UIKit

class AIViewModel: BattleViewModel, AIBehaviourManagerDelegate {
  
  private var playerOneZeroCardDamage = 0
  private var playerTwoZeroCardDamage = 0
  private var playerTwoLogicReference: AIBehaviourManager!
  
  /// Initializes the ViewModel and the BattleManager with values necessary to play the game
  ///
  /// - Parameters:
  ///   - playerOne: Deck for Player One
  ///   - playerTwo: Deck for Player Two
  override func initializeTheGame(playerOne: Deck, playerTwo: Deck) {
    bManager = BattleManager(playerOne: playerOne, playerTwo: playerTwo)
      playerTwoLogicReference = bManager.playerTwoLogicReference
      playerTwoLogicReference.delegate = self
    
    bManager.drawCardsFromDeck()
    updateData()
    notifyDelegateToReloadViewData()
    notifyDelegateToCreateInHandCards()
  }
  
  /// Toggle the turn for both the Players
  ///
  /// - Parameter forPlayerOne: Notifies if the turn to end is for Player One or Two
  override func toggleTurn(forPlayerOne: Bool) {
    endPlayerTurn(forPlayerOne: forPlayerOne)
    
    //Update Data and call delegates
    updateData()
    
    notifyDelegateToReloadViewData()
    notifyDelegateToDrawNewCardInHand()
  }
  
  private func endPlayerTurn(forPlayerOne: Bool) {
    if forPlayerOne {
      isPlayerTurn = bManager.endPlayerOneTurn()
      playerTwoZeroCardDamage += 1
      noCardDamageHandler(deckCount: playerTwoInDeckCards.count, stats: bManager.playerTwoStats, damage: playerTwoZeroCardDamage)
      bManager.playerTwoTurnStart()
    } else {
      isPlayerTurn = bManager.endPlayerTwoTurn()
      playerOneZeroCardDamage += 1
      noCardDamageHandler(deckCount: playerOneInDeckCards.count, stats: bManager.playerOneStats, damage: playerOneZeroCardDamage)
      bManager.playerOneTurnStart()
    }
  }
  
  private func noCardDamageHandler (deckCount: Int, stats: Stats, damage: Int) {
    if deckCount == 0 {
      stats.gameStats.getHurt(attackValue: damage)
    }
  }
  
  /// Starts the process for checking if the Player Two has any moves left (incase for AI) or passes the turn control to the other player via Peer to Peer
  override func startPlayerTwoChecks() {
    updateData()
    playerTwoLogicReference.attackWithACard()
    updateData()
  }
  
  
  /// Plays a Card for either Player One or Player Two, depending on whose turn it is
  ///
  /// - Parameter cardIndex: Index of the Cards from the InHand Array
  /// - Returns: Returns if the card play was successfull or failed
  override func playCard(cardIndex: Int) -> Bool {
    let success = bManager.playCard(cardIndex: cardIndex)
    if success {
      updateData()
      notifyDelegateToReloadViewData()
      return true
    } else {
      return false
    }
  }
  
  
  /// Makes a card attack the avatar directly
  ///
  /// - Parameter cardIndex: Index of the card from the InPlay Array of the active player
  override func performAttackOnAvatar(cardIndex: Int) {
    let success = bManager.attackAvatar(playerStats: bManager.playerOneStats, opponentStats: bManager.playerTwoStats, cardIndex: cardIndex)
    if success {
      updateData()
    }
  }
  
  
  /// Allows the card to attack another Card on the opponent side
  ///
  /// - Parameters:
  ///   - atkIndex: index of the Attacking card from the InPlay Array of the active player
  ///   - defIndex: index of the Defending card from the InPlay Array of the opponent
  override func performAttackOnCard(atkIndex: Int, defIndex: Int) -> Bool {
    let success = bManager.attackCard(attacker: playerOneInPlayCards[atkIndex], defender: playerTwoInPlayCards[defIndex], atkIndex: atkIndex, defIndex: defIndex)
    if success {
      updateData()
      return true
    }
    return false
  }

  
  //MARK: - Helpers
    
  

  //MARK: - Delegates Notifiers
  
  /// Notifies the ViewController to reload the UI Elements such as Labels, Indexes etc.
  func notifyDelegateToReloadViewData() {
    //Pass the message to ViewController to display required Data
      delegate?.reloadAllUIElements(self)
  }
  
  
  /// Notifies the ViewController to create In Hand Cards UI
  func notifyDelegateToCreateInHandCards() {
      delegate?.createInHandCards(self)
  }
  
  /// Notifies the ViewController to draw a New Card from the Deck
  func notifyDelegateToDrawNewCardInHand() {
      delegate?.drawANewCard(self)
  }

  //MARK: - Player Two Logic Delegates
  func attackAvatar(_ aIBehaviourManager: AIBehaviourManager, cardIndex: Int) {
    let success = bManager.attackAvatar(playerStats: bManager.playerTwoStats, opponentStats: bManager.playerOneStats, cardIndex: cardIndex)
    if success {
      updateData()
      delegate?.performCardToAvatarAttack(self, cardIndex: cardIndex)
    }
  }

  func attackAnotherCard(_ aiBehaviourManager: AIBehaviourManager, attacker: Card, defender: Card, atkIndex: Int, defIndex: Int) {
    let success = bManager.attackCard(attacker: attacker, defender: defender, atkIndex: atkIndex, defIndex: defIndex)
    if success {
      updateData()
      delegate?.performCardToCardAttack(self, atkIndex: atkIndex, defIndex: defIndex, atkCard: attacker, defCard: defender)
    }
  }

  func didSelectCardToPlay(_ aIBehaviourManager: AIBehaviourManager, cardIndex: Int) {
    updateData()
    delegate?.didSelectCardToPlay(self, cardIndex: cardIndex)
  }
  
  func didEndTurn(_ aIBehaviourManager: AIBehaviourManager) {
    toggleTurn(forPlayerOne: false)
    delegate?.playerTwoDidEndTurn(self)
  }

}
