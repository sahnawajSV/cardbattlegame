//
//  BattleViewModel.swift
//  CardBattleGame
//
//  Created by Vishal Aggarwal on 07/07/17.
//  Copyright © 2017 SAHNAWAJ BISWAS. All rights reserved.
//

import UIKit
/// Sends the required calls to the BattleViewController to perform UI Actions
protocol BattleDelegate: class {
  func reloadAllUIElements(_ battleViewModel: BattleViewModel)
  func createInHandCards(_ battleViewModel: BattleViewModel)
  func drawANewCard(_ battleViewModel: BattleViewModel)
  func performCardToCardAttack(_ battleViewModel: BattleViewModel, atkIndex: Int, defIndex: Int, atkCard: Card, defCard: Card)
  func performCardToAvatarAttack(_ battleViewModel: BattleViewModel, cardIndex: Int)
  func didSelectCardToPlay(_ battleViewModel: BattleViewModel, cardIndex: Int)
  func winLossConditionReached(_ battleViewModel: BattleViewModel, isVictorious: Bool)
  func playerTwoDidEndTurn(_ battleViewModel: BattleViewModel)
  func multiplayerConnecionLost(_ battleViewModel: BattleViewModel)
}


class BattleViewModel {
  weak var delegate: BattleDelegate?
  var bManager: BattleManager!
  var isPlayerTurn: Bool = false
  
  //MARK: - Used by ViewController
  //Player
  var playerOneName: String = ""
  var playerOneHealth: String = ""
  var playerOneNumOfCardsInDeckText: String = ""
  var playerOneTotalBattlePointsText: String = ""
  var playerOneNumOfCardsInDeck: Int = 0
  var playerOneTotalBattlePoints: Int = 0
  var playerOneInHandCards: [Card] = []
  var playerOneInDeckCards: [Card] = []
  var playerOneInPlayCards: [Card] = []
  
  //AI
  var playerTwoName: String = ""
  var playerTwoHealth: String = ""
  var playerTwoNumOfCardsInDeckText: String = ""
  var playerTwoTotalBattlePointsText: String = ""
  var playerTwoNumOfCardsInDeck: Int = 0
  var playerTwoTotalBattlePoints: Int = 0
  var playerTwoInHandCards: [Card] = []
  var playerTwoInDeckCards: [Card] = []
  var playerTwoInPlayCards: [Card] = []
  
  func initializeTheGame(playerOne: Deck, playerTwo: Deck) {
    
  }
  
  /// Toggle the turn for both the Players
  ///
  /// - Parameter forPlayerOne: Notifies if the turn to end is for Player One or Two
  func toggleTurn(forPlayerOne: Bool) {
    
  }
  
  
  /// Starts the process for checking if the Player Two has any moves left (incase for AI) or passes the turn control to the other player via Peer to Peer
  func startPlayerTwoChecks() {
    
  }
  
  
  /// Plays a Card for either Player One or Player Two, depending on whose turn it is
  ///
  /// - Parameter cardIndex: Index of the Cards from the InHand Array
  /// - Returns: Returns if the card play was successfull or failed
  func playCard(cardIndex: Int) -> Bool {
    return true
  }
  
  
  /// Makes a card attack the avatar directly
  ///
  /// - Parameter cardIndex: Index of the card from the InPlay Array of the active player
  func performAttackOnAvatar(cardIndex: Int) {
    
  }
  
  
  /// Allows the card to attack another Card on the opponent side
  ///
  /// - Parameters:
  ///   - atkIndex: index of the Attacking card from the InPlay Array of the active player
  ///   - defIndex: index of the Defending card from the InPlay Array of the opponent
  func performAttackOnCard(atkIndex: Int, defIndex: Int) -> Bool {
      return true
  }
  
  /// Model updates formatted into displayable formats
  func updateData() {
    playerOneName = bManager.playerOneStats.name
    playerOneHealth = String(bManager.playerOneStats.gameStats.health)
    playerTwoName = bManager.playerTwoStats.name
    playerTwoHealth = String(bManager.playerTwoStats.gameStats.health)
    
    playerOneNumOfCardsInDeckText = "\(String(describing: bManager.playerOneStats.gameStats.inDeck.count)) / \(Game.maximumCardPerDeck)"
    playerTwoNumOfCardsInDeckText = "\(String(describing: bManager.playerTwoStats.gameStats.inDeck.count)) / \(Game.maximumCardPerDeck)"
    playerOneTotalBattlePointsText = "\(String(describing: bManager.playerOneStats.gameStats.battlePoints)) / \(Game.maximumBattlePoint)"
    playerTwoTotalBattlePointsText = "\(String(describing: bManager.playerTwoStats.gameStats.battlePoints)) / \(Game.maximumBattlePoint)"
    
    playerOneNumOfCardsInDeck = bManager.playerOneStats.gameStats.inDeck.count
    playerTwoNumOfCardsInDeck = bManager.playerTwoStats.gameStats.inDeck.count
    playerOneTotalBattlePoints = bManager.playerOneStats.gameStats.battlePoints
    playerTwoTotalBattlePoints = bManager.playerTwoStats.gameStats.battlePoints
    
    playerOneInHandCards = bManager.playerOneStats.gameStats.inHand
    playerOneInDeckCards = bManager.playerOneStats.gameStats.inDeck
    playerOneInPlayCards = bManager.playerOneStats.gameStats.inPlay
    playerTwoInHandCards = bManager.playerTwoStats.gameStats.inHand
    playerTwoInDeckCards = bManager.playerTwoStats.gameStats.inDeck
    playerTwoInPlayCards = bManager.playerTwoStats.gameStats.inPlay
    
    //Set Turn Status
    isPlayerTurn = bManager.isPlayerTurn
    
    DispatchQueue.main.async {
      if self.bManager.playerOneStats.gameStats.health <= 0 {
        self.delegate?.winLossConditionReached(self, isVictorious: false)
      } else if self.bManager.playerTwoStats.gameStats.health <= 0 {
        self.delegate?.winLossConditionReached(self, isVictorious: true)
      }
    }
  }

  //Delegates
  func didEndTurn(_ p2pBehaviourManager: P2PBehaviourManager) {
    toggleTurn(forPlayerOne: false)
    delegate?.playerTwoDidEndTurn(self)
  }

}
