//
//  multiViewModel.swift
//  CardBattleGame
//
//  Created by Vishal Aggarwal on 01/08/17.
//  Copyright © 2017 SAHNAWAJ BISWAS. All rights reserved.
//

import UIKit


class P2PViewModel: BattleViewModel, P2PBehaviourManagerDelegate {
  private var playerOneZeroCardDamage = 0
  private var playerTwoZeroCardDamage = 0
  private var multiPlayerLogicReference: P2PBehaviourManager!
  
  
  /// Initializes the ViewModel and the BattleManager with values necessary to play the game
  ///
  /// - Parameters:
  ///   - playerOne: Deck for Player One
  ///   - playerTwo: Deck for Player Two
  override func initializeTheGame(playerOne: Deck, playerTwo: Deck) {
    bManager = BattleManager(playerOne: playerOne, playerTwo: playerTwo)
    multiPlayerLogicReference = P2PBehaviourManager(playerOneStats: bManager.playerOneStats, playerTwoStats: bManager.playerTwoStats)
      multiPlayerLogicReference.delegate = self
    
    bManager.drawCardsFromDeck()
    updateData()
    notifyDelegateToReloadViewData()
    notifyDelegateToCreateInHandCards()
  }
  
  /// Toggle the turn for both the Players
  ///
  /// - Parameter forPlayerOne: Notifies if the turn to end is for Player One or Two
  override func toggleTurn(forPlayerOne: Bool) {
    endPlayerTurn(forPlayerOne: forPlayerOne)
    
    //Update Data and call delegates
    updateData()
    
    notifyDelegateToReloadViewData()
    notifyDelegateToDrawNewCardInHand()
  }
  
  private func endPlayerTurn(forPlayerOne: Bool) {
    if forPlayerOne {
      isPlayerTurn = bManager.endPlayerOneTurn()
      if playerTwoInDeckCards.count == 0 {
        playerTwoZeroCardDamage += 1
        bManager.playerTwoStats.gameStats.getHurt(attackValue: playerTwoZeroCardDamage)
      }
      bManager.playerTwoTurnStart()
        startPlayerTwoChecks()
    } else {
      isPlayerTurn = bManager.endPlayerTwoTurn()
      if playerOneInDeckCards.count == 0 {
        playerOneZeroCardDamage += 1
        bManager.playerOneStats.gameStats.getHurt(attackValue: playerOneZeroCardDamage)
      }
      bManager.playerOneTurnStart()
    }
  }
  
  
  /// Starts the process for checking if the Player Two has any moves left (incase for AI) or passes the turn control to the other player via Peer to Peer
  override func startPlayerTwoChecks() {
    updateData()
        multiPlayerLogicReference.turnStart()
    updateData()
  }
  
  
  /// Plays a Card for either Player One or Player Two, depending on whose turn it is
  ///
  /// - Parameter cardIndex: Index of the Cards from the InHand Array
  /// - Returns: Returns if the card play was successfull or failed
  override func playCard(cardIndex: Int) -> Bool {
    let success = bManager.playCard(cardIndex: cardIndex)
    if success {
      updateData()
      notifyDelegateToReloadViewData()
          multiPlayerLogicReference.playACard(cardIndex: cardIndex)
      return true
    } else {
      return false
    }
  }
  
  
  /// Makes a card attack the avatar directly
  ///
  /// - Parameter cardIndex: Index of the card from the InPlay Array of the active player
  override func performAttackOnAvatar(cardIndex: Int) {
    let success = bManager.attackAvatar(playerStats: bManager.playerOneStats, opponentStats: bManager.playerTwoStats, cardIndex: cardIndex)
    if success {
          multiPlayerLogicReference.attackAvatar(cardIndex: cardIndex)
      updateData()
    }
  }
  
  
  /// Allows the card to attack another Card on the opponent side
  ///
  /// - Parameters:
  ///   - atkIndex: index of the Attacking card from the InPlay Array of the active player
  ///   - defIndex: index of the Defending card from the InPlay Array of the opponent
  override func performAttackOnCard(atkIndex: Int, defIndex: Int) -> Bool {
    let atkCard: Card = playerOneInPlayCards[atkIndex]
    let defCard: Card = playerTwoInPlayCards[defIndex]
    let success = bManager.attackCard(attacker: playerOneInPlayCards[atkIndex], defender: playerTwoInPlayCards[defIndex], atkIndex: atkIndex, defIndex: defIndex)
    if success {
          multiPlayerLogicReference.attackCard(fromIndex: atkIndex, toIndex: defIndex, atkCard: atkCard, defCard: defCard)
      updateData()
      return true
    }
    return false
  }


  //MARK: - Delegates Notifiers
  
  /// Notifies the ViewController to reload the UI Elements such as Labels, Indexes etc.
  func notifyDelegateToReloadViewData() {
    //Pass the message to ViewController to display required Data
    DispatchQueue.main.async {
      self.delegate?.reloadAllUIElements(self)
    }
  }
  
  
  /// Notifies the ViewController to create In Hand Cards UI
  func notifyDelegateToCreateInHandCards() {
    DispatchQueue.main.async {
      self.delegate?.createInHandCards(self)
    }
  }
  
  /// Notifies the ViewController to draw a New Card from the Deck
  func notifyDelegateToDrawNewCardInHand() {
    DispatchQueue.main.async {
      self.delegate?.drawANewCard(self)
    }
  }
  
  //MARK: P2P Behaviour Manager Delegates  
  func didSelectCardToPlay(_ p2pBehaviourManager: P2PBehaviourManager, cardIndex: Int) {
    let success = bManager.playCard(cardIndex: cardIndex)
    if success {
      updateData()
      DispatchQueue.main.async {
        self.delegate?.didSelectCardToPlay(self, cardIndex: cardIndex)
      }
    }
  }
  
  func attackAvatar(_ p2pBehaviourManager: P2PBehaviourManager, cardIndex: Int) {
    let success = bManager.attackAvatar(playerStats: bManager.playerTwoStats, opponentStats: bManager.playerOneStats, cardIndex: cardIndex)
    if success {
      updateData()
      DispatchQueue.main.async {
        self.delegate?.performCardToAvatarAttack(self, cardIndex: cardIndex)
      }
    }
  }
  
  func attackAnotherCard(_ p2pBehaviourManager: P2PBehaviourManager, attacker: Card, defender: Card, atkIndex: Int, defIndex: Int) {
    let success = bManager.attackCard(attacker: attacker, defender: defender, atkIndex: atkIndex, defIndex: defIndex)
    if success {
      updateData()
      DispatchQueue.main.async {
        self.delegate?.performCardToCardAttack(self, atkIndex: atkIndex, defIndex: defIndex, atkCard: attacker, defCard: defender)
      }
    }
  }
  
  func updateData(_ p2pBehaviourManager: P2PBehaviourManager) {
    updateData()
    notifyDelegateToReloadViewData()
  }
  
  func connectionLost(_ p2pBehaviourManager: P2PBehaviourManager) {
    
  }
}
