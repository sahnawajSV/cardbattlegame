//
//  P2PBehaviour.swift
//  CardBattleGame
//
//  Created by Vishal Aggarwal on 19/07/17.
//  Copyright © 2017 SAHNAWAJ BISWAS. All rights reserved.
//

import UIKit
import MultipeerConnectivity


/// Delegate to BattleViewModel
protocol P2PBehaviourManagerDelegate: class {
  func didEndTurn(_ p2pBehaviourManager: P2PBehaviourManager)
  func didSelectCardToPlay(_ p2pBehaviourManager: P2PBehaviourManager, cardIndex: Int)
  func attackAvatar(_ p2pBehaviourManager: P2PBehaviourManager, cardIndex: Int)
  func attackAnotherCard(_ p2pBehaviourManager: P2PBehaviourManager, attacker: Card, defender: Card, atkIndex: Int, defIndex: Int)
  func updateData(_ p2pBehaviourManager: P2PBehaviourManager)
  func connectionLost(_ p2pBehaviourManager: P2PBehaviourManager)
}


/// Handles the Battle Behaviour for Peer to Peer Multiplayer
class P2PBehaviourManager: P2PManagerDelegate {
  
  weak var delegate: P2PBehaviourManagerDelegate?
  
  var playerOneStats: Stats
  var playerTwoStats: Stats
  let p2pManager: P2PManager = P2PManager.sharedInstance
  
  init(playerOneStats: Stats, playerTwoStats: Stats) {
    self.playerOneStats = playerOneStats
    self.playerTwoStats = playerTwoStats
    p2pManager.delegate = self
    p2pManager.gameState = GameState.PlayGame
  }
  
  //MARK: - Action Methods
  func turnStart() {
    let turnStar: GameCommand = TurnStart()
    p2pManager.send(command: turnStar)
    allowAllPlayCardsToAttack()
  }
  
  func endTurn() {
    delegate?.didEndTurn(self)
  }
  
  func playACard(cardIndex: Int) {
    let playACard = PlayACard.init(cardIndex)
    p2pManager.send(command: playACard)
  }
  
  func didSelectCardToPlay(cardIndex: Int) {
    delegate?.didSelectCardToPlay(self, cardIndex: cardIndex)
  }
  
  func attackAvatar(cardIndex: Int) {
    let attackAvatar = AttackAvatar.init(cardIndex)
    p2pManager.send(command: attackAvatar)
  }
  
  func attack(avatar cardIndex: Int) {
    delegate?.attackAvatar(self, cardIndex: cardIndex)
  }
  
  func attackCard(attacker: Card, defender: Card, fromIndex: Int, toIndex: Int) {
    delegate?.attackAnotherCard(self, attacker: attacker, defender: defender, atkIndex: fromIndex, defIndex: toIndex)
  }
  
  func attackCard(fromIndex: Int, toIndex: Int, atkCard: Card, defCard: Card) {
    let attackCard = AttackCard.init(fromIndex: fromIndex, toIndex: toIndex, atkCard: atkCard, defCard: defCard)
    p2pManager.send(command: attackCard)
  }

  func allowAllPlayCardsToAttack() {
    let allCardsAttack = AllCardsAttack.init()
    p2pManager.send(command: allCardsAttack)
  }

  func p2pConnectionDisconnected() {
    delegate?.connectionLost(self)
  }
  
  //MARK: Delegate Methods
  func connectedDevicesChanged(_ manager: P2PManager, connectedDevices: [String], didChange state: MultiplayerConnectionState.GameState) {
    switch state {
    case .playerDisconnected:
      p2pConnectionDisconnected()
    case .gameEnded:
      print("Game Ended")
    default:
      print("Offline")
    }
  }
  
  func commandReceivedFromConnectedPlayer(_ manager: P2PManager, command: GameCommand) {
    command.execute(model: self)
    delegate?.updateData(self)
  }
  
  func notificationReceivedFromConnectedPlayer(_ manager: P2PManager, notification: MultiplayerData) {
  }
  
  func update(connection state: MultiplayerConnectionState.GameState) {
  }
}
