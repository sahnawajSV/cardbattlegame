//
//  BattleManager.swift
//  CardBattleGame
//
//  Created by Vishal Aggarwal on 07/07/17.
//  Copyright © 2017 SAHNAWAJ BISWAS. All rights reserved.
//

import UIKit

class BattleManager {
  //MARK: - Model Handlers
  var playerOneStats: Stats
  var playerTwoStats: Stats
  
  //Mark: - Game Handlers
  var isPlayerTurn: Bool = true
  
  //Player Two AI
  var playerTwoLogicReference: AIBehaviourManager? = nil
  
  //MARK: - Game Initialization
  
  /// Initialized the game required classes and variables
  ///
  /// - Parameters:
  ///   - playerOne: Deck for Player One
  ///   - playerTwo: Deck for Player Two
  ///   - isMultiplayer: checks if the game is multiplayer enabled or single player
  init(playerOne: Deck, playerTwo: Deck) {
    playerOneStats = Stats(name: "Player", id: "1", deckList: [playerOne], gameStats: Game(inDeck: playerOne.cardList, inHand: [], inPlay: [], battlePoints: Game.startingBattlePoints, health: Game.health))
    playerTwoStats = Stats(name: "AI", id: "2", deckList: [playerTwo], gameStats: Game(inDeck: playerTwo.cardList, inHand: [], inPlay: [], battlePoints: Game.startingBattlePoints, health: Game.health))
 
    playerTwoLogicReference = AIBehaviourManager(playerOneStats: playerOneStats, playerTwoStats: playerTwoStats)
  }
  
  /// Draw the initial number of cards from deck
  func drawCardsFromDeck() {
    drawPlayerOneCards(numToDraw: Game.numOfCardstoDrawInitially)
    drawPlayerTwoCards(numToDraw: Game.numOfCardstoDrawInitially)
  }
  
  //MARK: - ViewModel Calls
  
  /// End Player One Turn and toggles the required variable
  ///
  /// - Returns: Returns player turn value
  func endPlayerOneTurn() -> Bool {
    isPlayerTurn = false
    return isPlayerTurn
  }
  
  /// End Player Two Turn and toggles the required variable
  ///
  /// - Returns: Returns player turn value
  func endPlayerTwoTurn() -> Bool {
    isPlayerTurn = true
    return isPlayerTurn
  }
  
  //Logic Handlers
  
  /// Draws cards for Player One based on the number provided
  ///
  /// - Parameter numToDraw: number of cards to draw
  func drawPlayerOneCards(numToDraw: Int) {
    if (playerOneStats.gameStats.inDeck.count) > 0 {
      playerOneStats.gameStats.drawCards(numToDraw: numToDraw)
    }
  }
  
  /// Draws cards for Player Two based on the number provided
  ///
  /// - Parameter numToDraw: number of cards to draw
  func drawPlayerTwoCards(numToDraw: Int) {
    if (playerTwoStats.gameStats.inDeck.count) > 0 {
      playerTwoStats.gameStats.drawCards(numToDraw: numToDraw)
    }
  }
  
  
  /// Starts process required to enable Player Two Turn
  func playerTwoTurnStart() {
    playerTwoStats.gameStats.incrementTurn()
    playerTwoStats.gameStats.incrementBattlePoints()
    drawPlayerTwoCards(numToDraw: Game.cardDrawnPerTurn)
    if let playerTwoReference = playerTwoLogicReference {
      playerTwoReference.allowAllPlayCardsToAttack()
    }
  }
  
  /// Starts process required to enable Player One Turn
  func playerOneTurnStart() {
    playerOneStats.gameStats.incrementTurn()
    playerOneStats.gameStats.incrementBattlePoints()
    drawPlayerOneCards(numToDraw: 1)
    playerOneStats.gameStats.allowAllPlayCardsToAttack()
  }
  
  
  /// Card attacks the opponent avatar and updates the model classes
  ///
  /// - Parameters:
  ///   - playerStats: Stats for the active player
  ///   - opponentStats: Stats for the opponent
  ///   - cardIndex: index of the card InPlay for the active player
  func attackAvatar(playerStats: Stats, opponentStats: Stats, cardIndex: Int) -> Bool {
    let card: Card = playerStats.gameStats.inPlay[cardIndex]
    if card.canAttack {
      let attackValue = card.attack
      card.canAttack = false
      playerStats.gameStats.inPlay[cardIndex] = card
      opponentStats.gameStats.getHurt(attackValue: Int(attackValue))
      return true
    }
    return false
  }
  
  
  /// Card attacks another card and updates the model classes
  ///
  /// - Parameters:
  ///   - attacker: Card doing the attack
  ///   - defender: Card defending against the attack
  ///   - atkIndex: index of the Attack Card from InPlay of the active player
  ///   - defIndex: index of the defending Card from the InPlay of the opponent
  func attackCard(attacker: Card, defender: Card, atkIndex: Int, defIndex: Int) -> Bool {
    let atkCard = attacker
    if atkCard.canAttack {
      let defCard = defender
      atkCard.health = atkCard.health - defender.attack
      defCard.health = defender.health - atkCard.attack
      atkCard.canAttack = false
      if isPlayerTurn {
        performCardHealthCheck(forPlayer: playerOneStats, cardIndex: atkIndex, card: atkCard)
        performCardHealthCheck(forPlayer: playerTwoStats, cardIndex: defIndex, card: defCard)
      } else {
        performCardHealthCheck(forPlayer: playerTwoStats, cardIndex: atkIndex, card: atkCard)
        performCardHealthCheck(forPlayer: playerOneStats, cardIndex: defIndex, card: defCard)
      }
      return true
    }
    return false
  }
  
  
  /// Plays a card and updates the model
  ///
  /// - Parameter cardIndex: Index of the card from InHand of the active player
  /// - Returns: returns true if it was a successful play
  func playCard(cardIndex: Int) -> Bool {
    if isPlayerTurn {
      return updateBattlePoints(playerStats: playerOneStats, cardIndex: cardIndex)
    } else {
      return updateBattlePoints(playerStats: playerTwoStats, cardIndex: cardIndex)
    }
  }

  //Helpers
  private func performCardHealthCheck(forPlayer: Stats, cardIndex: Int, card: Card) {
    if card.health <= 0 {
      forPlayer.gameStats.inPlay.remove(at: cardIndex)
    } else {
      forPlayer.gameStats.inPlay[cardIndex] = card
    }
  }
  
  private func updateBattlePoints(playerStats: Stats, cardIndex: Int) -> Bool {
    if playerStats.gameStats.inPlay.count < 5 {
      if playerStats.gameStats.battlePoints >= Int(playerStats.gameStats.inHand[cardIndex].battlepoint) {
        playerStats.gameStats.playCard(cardIndex: cardIndex)
        return true
      } else {
        return false
      }
    } else {
      return false
    }
  }
}
