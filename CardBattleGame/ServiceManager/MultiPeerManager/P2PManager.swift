//
//  P2PManager.swift
//  CardBattleGame
//
//  Created by Vishal Aggarwal on 19/07/17.
//  Copyright © 2017 SAHNAWAJ BISWAS. All rights reserved.
//

import UIKit
import MultipeerConnectivity

enum GameState {
  case InvitePlayer
  case PlayGame
}

protocol P2PManagerDelegate {
  func connectedDevicesChanged(_ manager: P2PManager, connectedDevices: [String], didChange state: MultiplayerConnectionState.GameState)
  func notificationReceivedFromConnectedPlayer(_ manager: P2PManager, notification: MultiplayerData)
  func commandReceivedFromConnectedPlayer(_ manager: P2PManager, command: GameCommand)
}

class P2PManager: NSObject {
  
  static let sharedInstance = P2PManager()
  var gameState = GameState.InvitePlayer
  // Service type must be a unique string, at most 15 characters long
  // and can contain only ASCII lowercase letters, numbers and hyphens.
  private let gameServiceType = "card-battle"
  
  private let myPeerId = MCPeerID(displayName: "Assazzin")
  
  fileprivate let serviceAdvertiser : MCNearbyServiceAdvertiser
  fileprivate let serviceBrowser : MCNearbyServiceBrowser
  
  fileprivate let multiplayerConnectionState = MultiplayerConnectionState()
  
  var delegate : P2PManagerDelegate?
  
  var isServer : Bool = false
  
  lazy var session : MCSession = {
    let session = MCSession(peer: self.myPeerId, securityIdentity: nil, encryptionPreference: .none)
    session.delegate = self
    return session
  }()
  
  override init() {
    self.serviceAdvertiser = MCNearbyServiceAdvertiser(peer: myPeerId, discoveryInfo: nil, serviceType: gameServiceType)
    self.serviceBrowser = MCNearbyServiceBrowser(peer: myPeerId, serviceType: gameServiceType)
    super.init()
    multiplayerConnectionState.addObserver(self, forKeyPath: "state", options:.initial, context: nil)
  }
  
  override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
    if keyPath == "state" {
      if ((self.delegate) != nil) {
        self.delegate?.connectedDevicesChanged(self, connectedDevices:
          session.connectedPeers.map{$0.displayName}, didChange: multiplayerConnectionState.state)
      }
    }
  }
  
  func stopPeer2PeerService() {
    self.serviceBrowser.stopBrowsingForPeers()
    self.serviceAdvertiser.stopAdvertisingPeer()
  }
  
  func startPeer2PeerService() {
    multiplayerConnectionState.state = .searchOnlinePlayer
    
    self.serviceAdvertiser.delegate = self
    self.serviceAdvertiser.startAdvertisingPeer()
    
    self.serviceBrowser.delegate = self
    self.serviceBrowser.startBrowsingForPeers()
  }
  
  func send(data: MultiplayerData) {
    if session.connectedPeers.count > 0 {
      do {
        let archiveData: Data = NSKeyedArchiver.archivedData(withRootObject: data)
        try self.session.send(archiveData, toPeers: session.connectedPeers, with: .reliable)
      } catch {
        print("Error for sending")
      }
    }
  }
  
  func send(command: GameCommand) {
    if session.connectedPeers.count > 0 {
      do {
        let archiveData: Data = NSKeyedArchiver.archivedData(withRootObject: command)
        try self.session.send(archiveData, toPeers: session.connectedPeers, with: .reliable)
      } catch {
        print("Error for sending")
      }
    }
  }
}

extension P2PManager: MCNearbyServiceAdvertiserDelegate {
  
  func advertiser(_ advertiser: MCNearbyServiceAdvertiser, didNotStartAdvertisingPeer error: Error) {
  }
  
  func advertiser(_ advertiser: MCNearbyServiceAdvertiser, didReceiveInvitationFromPeer peerID: MCPeerID, withContext context: Data?, invitationHandler: @escaping (Bool, MCSession?) -> Void) {
    isServer = true
    multiplayerConnectionState.state = .foundOnlinePlayer
    stopPeer2PeerService()
    invitationHandler(true, self.session)
  }
  
}

extension P2PManager: MCNearbyServiceBrowserDelegate {
  
  func browser(_ browser: MCNearbyServiceBrowser, didNotStartBrowsingForPeers error: Error) {
    print(error.localizedDescription)
  }
  
  func browser(_ browser: MCNearbyServiceBrowser, foundPeer peerID: MCPeerID, withDiscoveryInfo info: [String : String]?) {
    multiplayerConnectionState.state = .foundOnlinePlayer
    stopPeer2PeerService()
    browser.invitePeer(peerID, to: self.session, withContext: nil, timeout: 10)
  }
  
  func browser(_ browser: MCNearbyServiceBrowser, lostPeer peerID: MCPeerID) {
    multiplayerConnectionState.state = .playerDisconnected
    print("Lost Peer")
  }
}

extension P2PManager: MCSessionDelegate {
  
  func session(_ session: MCSession, peer peerID: MCPeerID, didChange state: MCSessionState) {
    switch state {
    case .connected:
      multiplayerConnectionState.state = .playerConnected
    case .connecting:
      multiplayerConnectionState.state = .searchOnlinePlayer
    default:
      multiplayerConnectionState.state = .playerDisconnected
    }
  }
  
  func session(_ session: MCSession, didReceive data: Data, fromPeer peerID: MCPeerID) {
    switch gameState {
    case .InvitePlayer:
      if let data: MultiplayerData = NSKeyedUnarchiver.unarchiveObject(with: data) as? MultiplayerData {
        delegate?.notificationReceivedFromConnectedPlayer(self, notification: data)
      }
    default:
      if let data: GameCommand = NSKeyedUnarchiver.unarchiveObject(with: data) as? GameCommand {
        delegate?.commandReceivedFromConnectedPlayer(self, command: data)
      }
    }
  }
  
  func session(_ session: MCSession, didReceive stream: InputStream, withName streamName: String, fromPeer peerID: MCPeerID) {
    
  }
  
  func session(_ session: MCSession, didStartReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, with progress: Progress) {
  }
  
  func session(_ session: MCSession, didFinishReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, at localURL: URL?, withError error: Error?) {
  }
  
  func session(_ session: MCSession, didReceiveCertificate certificate: [Any]?, fromPeer peerID: MCPeerID, certificateHandler: @escaping (Bool) -> Void) {
    certificateHandler(true)
  }
}
