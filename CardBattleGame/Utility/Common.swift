//
//  Common.swift
//  CardBattleGame
//
//  Created by Vishal Aggarwal on 19/06/17.
//  Copyright © 2017 SAHNAWAJ BISWAS. All rights reserved.
//

import UIKit
import AVFoundation

func getRandomNumber(maxNumber: Int) -> Int {
  let randomNumber = arc4random_uniform(UInt32(maxNumber))
  return Int(randomNumber)
}

func generateRandomNumber(min: Int, max: Int) -> Int {
  let randomNum = Int(arc4random_uniform(UInt32(max) - UInt32(min)) + UInt32(min))
  return randomNum
}
