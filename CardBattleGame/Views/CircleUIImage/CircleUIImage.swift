//
//  CircleUIImage.swift
//  CardBattleGame
//
//  Created by SAHNAWAJ BISWAS on 20/07/17.
//  Copyright © 2017 SAHNAWAJ BISWAS. All rights reserved.
//

import UIKit

class CircleUIImage: UIImageView {
  
  override func layoutSubviews() {
    super.layoutSubviews()
    self.layer.cornerRadius = self.bounds.size.width/2
    self.layer.masksToBounds = true
    self.layer.borderWidth = 3
    self.layer.borderColor = UIColor.white.cgColor
  }
  
}
